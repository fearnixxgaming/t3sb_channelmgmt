package de.fearnixx.jeak.channelmgmt.commands;

import de.fearnixx.jeak.channelmgmt.enforcement.EnforcementService;
import de.fearnixx.jeak.channelmgmt.enforcement.IEnforcementService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.Optional;

public class EnforceRegisterCommand implements ICommandReceiver {

    @Inject
    public IEnforcementService service;

    @Inject
    public IDataCache dataCache;

    @Override
    public void receive(ICommandContext ctx) throws CommandException {
        if (ctx.getTargetType() != TargetType.CLIENT) {
            throw new CommandException("Non-private chat used!");
        }

        final Optional<String> optClientId = ctx.getRawEvent().getProperty(PropertyKeys.TextMessage.SOURCE_ID);
        if (optClientId.isEmpty()) {
            throw new CommandException("No client ID available!");
        }

        Integer clientId = Integer.parseInt(optClientId.get());
        final IClient sender = dataCache.getClientMap().getOrDefault(clientId, null);

        if (sender == null) {
            throw new CommandException("Client id not in cache.");
        }
        if (!sender.hasPermission("channelmgmt.command.enforce_register")) {
            throw new CommandException("You are not allowed to use this command!");
        }

        final Integer channelId = sender.getChannelID();

        if (ctx.getArguments().size() != 1) {
            throw new CommandException("Only one argument expected! (Set name)");
        }

        String setName = ctx.getArguments().get(0);
        if (!service.hasSet(setName)) {
            throw new CommandParameterException("Unknown set name given.", "Set name", setName);
        }

        service.enableSetOn(channelId, setName);
        ctx.getRawEvent().getConnection()
                .sendRequest(sender.sendMessage(String.format("Channel %s added to set %s", channelId, setName)));
    }

    public void setService(EnforcementService service) {
        this.service = service;
    }
}
