package de.fearnixx.jeak.channelmgmt.commands;

import de.fearnixx.jeak.channelmgmt.overflow.OverflowManager;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class OverflowRegisterCommand implements ICommandReceiver {

    @Inject
    public IDataCache dataCache;

    private OverflowManager manager;

    @Override
    public void receive(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().size() != 1) {
            throw new CommandParameterException("Command takes only 1 parameter (set name).");
        }

        if (ctx.getTargetType() != TargetType.CLIENT) {
            throw new CommandParameterException("Command only accepts private chat!");
        }

        IQueryEvent.INotification.IClientTextMessage msg = (IQueryEvent.INotification.IClientTextMessage) ctx.getRawEvent();
        Integer invokerId = msg.getProperty(PropertyKeys.TextMessage.SOURCE_ID)
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Message does not have an invoker ID!"));

        IClient sender = dataCache.getClientMap().getOrDefault(invokerId, null);
        if (sender == null) {
            throw new RuntimeException("Failed to get invoker client!");
        }
        if (!sender.hasPermission("channelmgmt.command.overflow_register") && !manager.isAdmin(sender.getClientUniqueID())) {
            throw new CommandException("You are not allowed to use this command!");
        }

        String setName = ctx.getArguments().get(0);
        if (!manager.hasSet(setName)) {
            throw new CommandParameterException("Unable to find set.", "setName", setName);
        }

        Integer channelId = sender.getChannelID();
        manager.enableSetOn(setName, channelId);
        ctx.getRawEvent().getConnection().sendRequest(
                sender.sendMessage("Successfully added channel to set")
        );
    }

    public void setManager(OverflowManager manager) {
        this.manager = manager;
    }
}
