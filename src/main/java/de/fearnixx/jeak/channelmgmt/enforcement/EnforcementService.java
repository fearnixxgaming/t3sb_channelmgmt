package de.fearnixx.jeak.channelmgmt.enforcement;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.Config;
import de.fearnixx.jeak.reflect.IInjectionService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.IServiceManager;
import de.fearnixx.jeak.service.event.IEventService;
import de.mlessmann.confort.LoaderFactory;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.except.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Consumer;

public class EnforcementService implements IEnforcementService {

    private static final Logger logger = LoggerFactory.getLogger(EnforcementService.class);
    public final String DEFAULT_CONFIG_PATH = "/channelmgmt/enforcement_default.json";
    public final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    @Inject
    public IServiceManager serviceManager;

    @Inject
    public IInjectionService injectionService;

    @Inject
    public IEventService eventService;

    @Inject
    @Config(id = "enforcement")
    public IConfig configRef;
    private IConfigNode config;

    private final Map<String, EnforcementSet> enforcementSets = new HashMap<>();
    private final Map<Integer, List<EnforcementSet>> enforcedChannels = new HashMap<>();

    private final InitialCheckListener initialCheckListener = new InitialCheckListener();
    private final EnforcementWatcher watcher = new EnforcementWatcher();

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
            return;
        }
        config.getNode("sets")
                .optMap()
                .orElseGet(Collections::emptyMap)
                .entrySet()
                .stream()
                .filter(e -> e.getValue().isMap())
                .forEach(e -> addSet(e.getKey(), e.getValue()));

        config.getNode("channels")
                .optMap()
                .orElseGet(Collections::emptyMap)
                .entrySet()
                .stream()
                .filter(e -> e.getValue().isList())
                .forEach(e -> addChannel(e.getKey(), e.getValue()));


        serviceManager.registerService(IEnforcementService.class, this);
        injectionService.injectInto(watcher);
        eventService.registerListener(watcher);

        injectionService.injectInto(initialCheckListener);
        eventService.registerListener(initialCheckListener);
    }

    private boolean loadConfig() {
        try {
            configRef.load();
            config = configRef.getRoot();
        } catch (ParseException | IOException e) {
            logger.error("Failed to load configuration!", e);
            return false;
        }

        if (config.isVirtual()) {
            return loadDefault();
        }
        return true;
    }

    private boolean loadDefault() {
        try (InputStream in = EnforcementService.class.getResourceAsStream(DEFAULT_CONFIG_PATH)) {
            if (in == null) {
                throw new IOException("Resource not found: " + DEFAULT_CONFIG_PATH);
            }
            InputStreamReader reader = new InputStreamReader(in, DEFAULT_CHARSET);

            URI sourceLocator = new URI("classpath:" + DEFAULT_CONFIG_PATH);
            final IConfigNode defaultRoot = LoaderFactory.getLoader("application/json").parse(reader, sourceLocator);
            configRef.setRoot(defaultRoot);
            config = configRef.getRoot();
            return saveConfig();

        } catch (IOException | ParseException | URISyntaxException e) {
            logger.error("Failed to load default configuration!", e);
            return false;
        }
    }

    private boolean saveConfig() {
        try {
            configRef.save();
            return true;
        } catch (IOException e) {
            logger.warn("Failed to save configuration!", e);
            return false;
        }
    }

    private void addSet(String name, IConfigNode src) {
        if (enforcementSets.containsKey(name)) {
            logger.warn("Duplicate set names for name {} ???", name);
        }

        EnforcementSet set = new EnforcementSet();
        src.optMap()
                .orElseGet(Collections::emptyMap)
                .entrySet()
                .stream()
                .filter(e -> !e.getKey().startsWith(":"))
                .forEach(e -> set.enforce(e.getKey(), e.getValue().asString()));

        enforcementSets.put(name, set);
    }

    private void addChannel(String channelIdStr, IConfigNode src) {
        Integer channelId;
        try {
            channelId = Integer.parseInt(channelIdStr);
        } catch (NumberFormatException e) {
            logger.error("Invalid channel ID: {}", channelIdStr, e);
            return;
        }

        src.optList()
                .orElseGet(Collections::emptyList)
                .forEach(set -> enableSetOn(channelId, set.asString(), false));
    }

    @Override
    public boolean hasSet(String setName) {
        return enforcementSets.containsKey(setName);
    }

    @Override
    public void enableSetOn(Integer channelId, String setName) {
        enableSetOn(channelId, setName, true);
    }

    private void enableSetOn(Integer channelId, String setName, boolean flush) {
        if (!enforcementSets.containsKey(setName)) {
            logger.warn("Unable to find set with name \"{}\" for channel id: {}", setName, channelId);
        } else {
            final EnforcementSet enforcementSet = enforcementSets.get(setName);
            final List<EnforcementSet> enabledSets = enforcedChannels.computeIfAbsent(channelId, id -> new LinkedList<>());

            if (!enabledSets.contains(enforcementSet)) {
                enabledSets.add(enforcementSet);

                if (flush) {
                    final IConfigNode listEntry = config.createNewInstance();
                    listEntry.setString(setName);
                    config.getNode("channels", channelId.toString()).append(listEntry);
                    saveConfig();
                    runForChannel(channelId);
                }
            }
        }
    }

    public boolean isWatched(Integer channelId) {
        return enforcedChannels.containsKey(channelId) && !enforcedChannels.get(channelId).isEmpty();
    }

    @Override
    public void runForChannel(Integer channelId) {
        watcher.enforce(channelId);
    }

    public void runForChannel(Integer channelId, Consumer<EnforcementSet> action) {
        if (enforcedChannels.containsKey(channelId)) {
            enforcedChannels.get(channelId).forEach(action);
        }
    }

    @Override
    public Set<Integer> getWatchedChannels() {
        return Collections.unmodifiableSet(enforcedChannels.keySet());
    }

    @Override
    public void unwatch(Integer channel) {
        if (isWatched(channel)) {
            enforcedChannels.remove(channel);
            config.getNode("channels").remove(channel.toString());
            saveConfig();
        }
    }

    @Listener
    public void onChannelDeleted(IQueryEvent.INotification.IChannelDeleted event) {
        unwatch(event.getTarget().getID());
    }
}
