package de.fearnixx.jeak.channelmgmt.enforcement;

import java.util.Set;
import java.util.function.Consumer;

public interface IEnforcementService {

    boolean hasSet(String setName);

    void enableSetOn(Integer channelId, String setName);

    boolean isWatched(Integer channelId);

    void runForChannel(Integer channelId, Consumer<EnforcementSet> action);

    void runForChannel(Integer channelId);

    Set<Integer> getWatchedChannels();

    void unwatch(Integer channel);
}
