package de.fearnixx.jeak.channelmgmt.enforcement;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IChannel;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Runs enforcements once on startup, then unregisters itself automatically.
 * Will also delete watched channels that don't exist anymore.
 */
public class InitialCheckListener {

    @Inject
    public IDataCache dataCache;

    @Inject
    public IEventService eventService;

    @Inject
    public IEnforcementService enforcementService;

    @Listener(order = Listener.Orders.EARLY)
    public void onChannelsUpdated(IQueryEvent.IDataEvent.IRefreshChannels event) {
        final Map<Integer, IChannel> channels = dataCache.getChannelMap();
        final List<Integer> unwatch = new LinkedList<>();
        enforcementService.getWatchedChannels().forEach(c -> {
            if (channels.containsKey(c)) {
                enforcementService.runForChannel(c);
            } else {
                unwatch.add(c);
            }
        });
        unwatch.forEach(enforcementService::unwatch);
        eventService.unregisterListener(this);
    }
}
