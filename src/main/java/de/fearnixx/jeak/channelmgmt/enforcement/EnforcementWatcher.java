package de.fearnixx.jeak.channelmgmt.enforcement;

import de.fearnixx.jeak.event.IQueryEvent.INotification;
import de.fearnixx.jeak.event.IRawQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.teamspeak.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class EnforcementWatcher {

    private static final Logger logger = LoggerFactory.getLogger(EnforcementWatcher.class);

    @Inject
    private IEnforcementService service;

    @Inject
    private IDataCache dataCache;

    @Inject
    private IUserService userService;

    @Inject
    private IServer server;

    @Listener
    public void onChannelEdited(INotification.IChannelEdited event) {
        if (!service.isWatched(event.getTarget().getID())) {
            return;
        }

        // If we were the editors, run no checks.
        String ownId = server.getConnection().getWhoAmI().getProperty("client_id").orElse("");
        String invokerId = event.getProperty(PropertyKeys.TextMessage.SOURCE_ID).orElse(null);
        if (ownId.equals(invokerId)) {
            logger.debug("Nothing to enforce. Was me.");
            return;
        } else if (invokerId != null) {
            final IClient client = userService.getClientByID(Integer.parseInt(invokerId))
                    .orElse(null);
            if (client != null) {
                if (client.hasPermission("channelmgmt.enforce.ignore_edit")) {
                    logger.info("Channel-edit ignored for permitted client: {}", client);
                    return;
                }
            }
        }

        if (event instanceof INotification.IChannelEditedDescription) {
            onDescriptionChanged(event);
        } else if (event instanceof INotification.IChannelPasswordChanged) {
            onPasswordChanged(event);
        } else {
            onGenericChange(event);
        }
    }

    private void onDescriptionChanged(INotification.IChannelEdited event) {
        enforceNonDeltaProperty(event, PropertyKeys.Channel.DESCRIPTION);
    }

    private void onPasswordChanged(INotification.IChannelEdited event) {
        enforceNonDeltaProperty(event, PropertyKeys.Channel.PASSWORD);
    }

    private void enforceNonDeltaProperty(INotification.IChannelEdited event, String property) {
        final IChannel target = event.getTarget();
        EnforcementAction action = new EnforcementAction();

        service.runForChannel(target.getID(), set -> {
            if (set.getEnforcedProperties().containsKey(property)) {
                final String desiredValue = set.getEnforcedProperties().get(property);
                action.enforce(property, desiredValue);
            }
        });
        runEnforceAction(target.getID(), action);
    }

    private void onGenericChange(INotification.IChannelEdited event) {
        IChannel target = event.getTarget();
        EnforcementAction action = new EnforcementAction();
        service.runForChannel(target.getID(), set -> {
            event.getChanges().forEach((key, value) -> {
                if (set.getEnforcedProperties().containsKey(key)) {
                    logger.debug("Enforcing propery \"{}\" on \"{}\"", key, target);
                    action.enforce(key, set.getEnforcedProperties().get(key));
                }
            });
        });
        runEnforceAction(target.getID(), action);
    }

    public void enforce(Integer channelId) {
        IDataHolder channel = dataCache.getChannelMap().getOrDefault(channelId, null);
        if (channel == null) {
            logger.debug("Running enforcements on non-cached channel: {}", channelId);
            channel = new BasicDataHolder();
        }
        checkChannel(channelId, channel, true);
    }

    @SuppressWarnings("squid:S1602")
    private void checkChannel(Integer channelId, IDataHolder channel, boolean mirror) {
        EnforcementAction action = new EnforcementAction();

        //noinspection CodeBlock2Expr
        service.runForChannel(channelId, set -> {
            set.getEnforcedProperties().forEach((enforcedProperty, enforcedValue) -> {
                Optional<String> optProp = channel.getProperty(enforcedProperty);

                if (optProp.isPresent()) {
                    if (!optProp.get().equals(enforcedValue)) {
                        action.enforce(enforcedProperty, enforcedValue);
                    }
                } else if (mirror) {
                    action.enforce(enforcedProperty, enforcedValue);
                } else {
                    logger.info("Unmatched enforcement property: {}", enforcedProperty);
                }
            });
        });
        runEnforceAction(channelId, action);
    }

    private void runEnforceAction(Integer channelId, EnforcementAction action) {
        if (!action.getValuesToEnforce().isEmpty()) {
            QueryBuilder command = IQueryRequest.builder().command("channeledit");
            action.getValuesToEnforce().forEach(command::addKey);
            command.addKey(PropertyKeys.Channel.ID, channelId);

            command.onSuccess(answer -> logger.debug("Successfully enforced properties for channel: {}", channelId));
            command.onError(answer -> {
                final IRawQueryEvent.IMessage.IErrorMessage error = answer.getError();
                logger.warn("Failed to enforce properties for channel: {} - {} {}", channelId, error.getCode(), error.getMessage());
            });

            server.getConnection().sendRequest(command.build());
        }
    }
}
