package de.fearnixx.jeak.channelmgmt.enforcement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EnforcementAction {

    private Map<String, String> properties = new HashMap<>();

    public void enforce(String property, String value) {
        Objects.requireNonNull(property, "Property key for enforcement may not be null!");

        if (value == null) {
            value = "";
        }

        properties.put(property, value);
    }

    public Map<String, String> getValuesToEnforce() {
        return Collections.unmodifiableMap(properties);
    }
}
