package de.fearnixx.jeak.channelmgmt.enforcement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EnforcementSet {

    private final Map<String, String> enforcedProperties = new HashMap<>();

    public Map<String, String> getEnforcedProperties() {
        return Collections.unmodifiableMap(enforcedProperties);
    }

    public void enforce(String property, String value) {
        if (value == null) {
            enforcedProperties.remove(property);
        } else {
            enforcedProperties.put(property, value);
        }
    }
}
