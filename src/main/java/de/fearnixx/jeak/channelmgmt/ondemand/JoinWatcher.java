package de.fearnixx.jeak.channelmgmt.ondemand;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class JoinWatcher {

    private static final Logger logger = LoggerFactory.getLogger(JoinWatcher.class);

    private ChannelDemandService demandService;

    public JoinWatcher(ChannelDemandService demandService) {
        this.demandService = demandService;
    }

    @Listener
    public void onClientMoved(IQueryEvent.INotification.IClientMoved clientMoved) {
        Integer channelId = clientMoved.getTargetChannelId();
        demandService.getSetForWatchedChannel(channelId)
                .ifPresent(set -> {
                    logger.debug("Demand set found for channel: {}: {} for user {}", clientMoved.getTargetChannelId(), set.getName(), clientMoved.getTarget());
                    demandService.demandChannel(set, clientMoved.getTarget());
                });
    }
}
