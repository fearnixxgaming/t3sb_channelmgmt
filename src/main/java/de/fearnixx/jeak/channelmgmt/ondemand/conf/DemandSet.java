package de.fearnixx.jeak.channelmgmt.ondemand.conf;

import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class DemandSet {

    private static final int DEFAULT_CHANNEL_ADMIN_GROUP = 5;
    private static final Logger logger = LoggerFactory.getLogger(DemandSet.class);

    private final String name;
    private List<Integer> allowedServerGroups = new ArrayList<>();
    private int joinWatchChannel;

    private Integer subjectChannelGroup;
    private int subjectParent;
    private int subjectTTL;
    private IChannel.ChannelPersistence subjectType;
    private Map<String, String> subjectProperties = new HashMap<>();
    private Map<String, Integer> subjectPermissions = new HashMap<>();

    private int groupAtCount = 0;
    private String groupDelimNamePattern;
    private Map<String, String> groupDelimProperties = new HashMap<>();
    private boolean persistChanges;

    public DemandSet(String name) {
        this.name = name;
    }

    public boolean load(IConfigNode node) {
        allowedServerGroups.clear();

        node.getNode("allowedGroups")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .map(IValueHolder::asInteger)
                .forEach(allowedServerGroups::add);

        joinWatchChannel = node.getNode("watch_channel")
                .optInteger(-1);
        IConfigNode subjectNode = node.getNode("channel");
        subjectParent = subjectNode.getNode("parent")
                .optInteger(0);
        subjectTTL = subjectNode.getNode("ttl")
                .optInteger(300);
        subjectType = translateType(subjectNode.getNode("type")
                .optString("temporary"));
        subjectChannelGroup = subjectNode.getNode("channel_group")
                .optInteger(DEFAULT_CHANNEL_ADMIN_GROUP);
        persistChanges = subjectNode.getNode("rememberChanges")
                .optBoolean(false);

        groupAtCount = node.getNode("group_at")
                .optInteger(0);
        groupDelimNamePattern = node.getNode("group_delimiter", "name")
                .optString("[cspacer] == Block %1% ==");

        subjectNode.getNode("properties")
                .optValueMap(String.class)
                .forEach(subjectProperties::put);
        subjectNode.getNode("permissions")
                .optValueMap(Integer.class)
                .forEach(subjectPermissions::put);

        node.getNode("group_delimiter", "properties")
                .optMap()
                .orElseGet(Collections::emptyMap)
                .forEach((k, v) -> groupDelimProperties.put(k, v.asString()));

        boolean valid = true;
        if (subjectParent < 0 || (subjectType.equals(IChannel.ChannelPersistence.TEMPORARY) && subjectTTL < 0)) {
            logger.warn("Illegal subject settings for on-demand channel!");
            valid = false;
        }

        if (groupAtCount > 0 && !groupDelimNamePattern.contains("%i%")) {
            logger.warn("Group delimiter does not contain a \"%i%\" for number-insertion!");
            valid = false;
        }

        return valid;
    }

    private IChannel.ChannelPersistence translateType(String type) {
        switch (type) {
            case "temporary":
            case "temp":
                return IChannel.ChannelPersistence.TEMPORARY;
            case "permanent":
            case "perm":
                return IChannel.ChannelPersistence.PERMANENT;
            case "semi-permanent":
            case "semi-perm":
                return IChannel.ChannelPersistence.SEMI_PERMANENT;
            default:
                throw new IllegalArgumentException("Unknown channel persistence type: " + type);
        }
    }

    public String getName() {
        return name;
    }

    public List<Integer> getAllowedServerGroups() {
        return allowedServerGroups;
    }

    public int getJoinWatchChannel() {
        return joinWatchChannel;
    }

    public int getSubjectParent() {
        return subjectParent;
    }

    public int getSubjectTTL() {
        return subjectTTL;
    }

    public IChannel.ChannelPersistence getSubjectType() {
        return subjectType;
    }

    public Map<String, String> getSubjectProperties() {
        return subjectProperties;
    }

    public Map<String, Integer> getSubjectPermissions() {
        return subjectPermissions;
    }

    public boolean isPersistChanges() {
        return persistChanges;
    }

    public int getGroupAtCount() {
        return groupAtCount;
    }

    public String getGroupDelimNamePattern() {
        return groupDelimNamePattern;
    }

    public Map<String, String> getGroupDelimProperties() {
        return groupDelimProperties;
    }

    public Integer getSubjectChannelGroup() {
        return subjectChannelGroup;
    }
}
