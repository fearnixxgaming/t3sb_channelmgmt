package de.fearnixx.jeak.channelmgmt.ondemand;

import de.fearnixx.jeak.channelmgmt.ondemand.conf.DemandSet;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class DemandChannelCommand implements ICommandReceiver {

    private ChannelDemandService demandService;

    @Inject
    private IUserService userService;

    public DemandChannelCommand(ChannelDemandService demandService) {
        this.demandService = demandService;
    }

    @Override
    public void receive(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().size() != 1) {
            throw new CommandException("Invalid argument count! Only need the set name.");
        }
        String inputSetName = ctx.getArguments().get(0);
        DemandSet demandSet = demandService.getSetByName(inputSetName)
                .orElse(null);

        if (demandSet == null) {
            throw new CommandParameterException("Unknown demand set name given.", "set-name", inputSetName);
        }

        IClient invoker = userService.getClientByID(ctx.getRawEvent().getInvokerId())
                .orElseThrow(() -> new IllegalStateException("Demand-command invoker is not cached!"));
        demandService.demandChannel(demandSet, invoker);
    }
}
