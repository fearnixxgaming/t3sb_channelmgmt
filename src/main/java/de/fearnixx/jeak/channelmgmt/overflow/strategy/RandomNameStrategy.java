package de.fearnixx.jeak.channelmgmt.overflow.strategy;

import de.fearnixx.jeak.channelmgmt.overflow.OverflowManager;
import de.fearnixx.jeak.channelmgmt.overflow.state.NameTrackingState;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RandomNameStrategy extends OverflowStrategy<NameTrackingState> {

    private static final String TOPIC_PREFIX = "RNS/UUID/";
    private static final String UUID_PATTERN = "\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b";
    private static final Pattern UUID_TOPIC_PATTERN = Pattern.compile(TOPIC_PREFIX + "(" + UUID_PATTERN + ")");

    private static final Logger logger = LoggerFactory.getLogger(RandomNameStrategy.class);

    private static final Random GENERATOR = new Random();

    private OverflowManager manager;

    private UUID uuid;
    private final List<String> names = new LinkedList<>();
    private boolean useStrictMatching;
    private Integer maxChildren;

    public RandomNameStrategy(OverflowManager manager) {
        this.manager = manager;
    }

    @Override
    public boolean fromNode(IConfigNode node) {

        String uuidSource = node.getNode("uuid").optString().orElseGet(() -> UUID.randomUUID().toString());
        if (node.getNode("uuid").isVirtual()) {
            node.getNode("uuid").setString(uuidSource);
            manager.saveAfterInit();
            logger.info("Generated new UUID.");
        }

        try {
            uuid = UUID.fromString(uuidSource);
        } catch (IllegalArgumentException e) {
            logger.error("Invalid UUID found in set UUID - delete the key if the cause is not known.");
            return false;
        }

        try {
            if (node.getNode("names").isList()) {
                node.getNode("names").asList()
                        .stream()
                        .filter(IConfigNode::isPrimitive)
                        .map(item -> item.optString()
                                .orElseThrow(() -> new IllegalArgumentException("Items in \"names\" must be strings!")))
                        .forEach(names::add);
            }
        } catch (IllegalArgumentException e) {
            logger.error("Failed to load names for set!", e);
            return false;
        }

        useStrictMatching = node.getNode("strictMatching").optBoolean(false);
        maxChildren = node.getNode("maxChildren").optInteger(names.size());

        return super.fromNode(node);
    }

    @Override
    protected boolean matches(IChannel child) {
        Matcher matcher = UUID_TOPIC_PATTERN.matcher(child.getProperty(PropertyKeys.Channel.TOPIC).get());
        try {
            return matcher.find() && matcher.groupCount() == 1
                    && UUID.fromString(matcher.group(1)).equals(uuid);
        } catch (IllegalArgumentException e) {
            logger.warn("Failed to match channel {}/{}: Invalid UUID stored!", child.getName(), child.getID());
            return false;
        }
    }

    @Override
    protected NameTrackingState createState(IChannel parent) {
        return new NameTrackingState(parent);
    }

    @Override
    protected Optional<IDataHolder> createChannel(NameTrackingState state) {
        Optional<String> optName = getName(state.getChannelsTracked());

        if (optName.isPresent()) {
            IDataHolder channelProps = new BasicDataHolder().copyFrom(getChannelProperties());
            channelProps.setProperty(PropertyKeys.Channel.PARENT, state.getParent().getID());
            channelProps.setProperty(PropertyKeys.Channel.NAME, optName.get());

            StringBuilder descrBuilder = new StringBuilder(
                    channelProps.getProperty(PropertyKeys.Channel.TOPIC).orElse(""));

            // Use 10 line breaks to separate the property marker
            for (int i = 0; i < 10; i++) {
                descrBuilder.append('\n');
            }
            descrBuilder.append(TOPIC_PREFIX).append(uuid.toString());
            channelProps.setProperty(PropertyKeys.Channel.TOPIC, descrBuilder.toString());

            List<IChannel> trackedChildren = state.getChannelsTracked();

            // Set sorting ID
            Integer sortBy;
            if (trackedChildren.isEmpty()) {
                sortBy = 0;
            } else {
                sortBy = getLastChannel(trackedChildren)
                        .map(IChannel::getID)
                        .orElseGet(() -> {
                            logger.debug("No last channel found, using top-level.");
                            return 0;
                        });
            }
            channelProps.setProperty(PropertyKeys.Channel.ORDER, sortBy);
            return Optional.of(channelProps);

        } else {
            logger.warn("Cannot create any more channels for parent {}/{}", state.getParent().getName(), state.getParent().getID());
            return Optional.empty();
        }
    }

    protected Optional<String> getName(List<IChannel> trackedChannels) {
        if (trackedChannels.size() < maxChildren) {

            // Extract predicate from next lambda to avoid ugly indentation.
            BiPredicate<String, String> nameMatcher = (name, cand)
                    -> useStrictMatching ? name.equals(cand) : name.contains(cand);

            List<String> freeNames = names.stream()
                    .filter(candidate ->
                            trackedChannels.stream()
                                    .map(IChannel::getName)
                                    .noneMatch(name -> nameMatcher.test(name, candidate)))
                    .collect(Collectors.toList());

            if (!freeNames.isEmpty()) {
                return Optional.of(freeNames.get(GENERATOR.nextInt(freeNames.size())));
            }
        }

        return Optional.empty();
    }

    protected Optional<IChannel> getLastChannel(List<IChannel> trackedChannels) {
        return trackedChannels
                .stream()
                .filter(channel -> {
                    Integer channelId = channel.getID();
                    return trackedChannels
                            .stream()
                            .map(IChannel::getParent)
                            .noneMatch(channelId::equals);
                }).findFirst();
    }
}
