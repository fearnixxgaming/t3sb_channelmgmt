package de.fearnixx.jeak.channelmgmt.overflow.state;


import de.fearnixx.jeak.teamspeak.data.IChannel;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class OverflowState {

    private final AtomicInteger freeChildren = new AtomicInteger();
    private final AtomicInteger numberOfChildren = new AtomicInteger();
    private final AtomicInteger occupiedChildren = new AtomicInteger();
    private final List<IChannel> channelsTracked = Collections.synchronizedList(new LinkedList<>());

    private final IChannel parent;

    public OverflowState(IChannel parent) {
        this.parent = parent;
    }

    public AtomicInteger freeChildren() {
        return freeChildren;
    }

    public AtomicInteger numberOfChildren() {
        return numberOfChildren;
    }

    public AtomicInteger occupiedChildren() {
        return occupiedChildren;
    }

    public IChannel getParent() {
        return parent;
    }

    public List<IChannel> getChannelsTracked() {
        return channelsTracked;
    }

    public void trackChannel(IChannel child) {
        if (!channelsTracked.contains(child)) {
            channelsTracked.add(child);
        }
    }

    public void untrackChannel(Integer cid) {
        channelsTracked.removeIf(channel -> channel.getID().equals(cid));
    }
}
