package de.fearnixx.jeak.channelmgmt.overflow.state;


import de.fearnixx.jeak.teamspeak.data.IChannel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CountingState extends OverflowState {

    private final Map<Integer, Integer> assignmentCache = Collections.synchronizedMap(new HashMap<>());

    public CountingState(IChannel parent) {
        super(parent);
    }

    public void assignNumber(Integer number, Integer channelId) {
        assignmentCache.put(number, channelId);
    }

    public void untrack(Integer channelId) {
        assignmentCache.entrySet().removeIf(e -> e.getValue().equals(channelId));
    }

    public Integer getChannelIdForNumber(Integer number) {
        return assignmentCache.getOrDefault(number, null);
    }

    public Set<Integer> getUsedNumbers() {
        return assignmentCache.keySet();
    }
}
