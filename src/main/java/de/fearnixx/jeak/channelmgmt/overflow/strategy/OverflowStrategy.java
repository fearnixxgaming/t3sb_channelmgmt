package de.fearnixx.jeak.channelmgmt.overflow.strategy;

import de.fearnixx.jeak.channelmgmt.overflow.OverflowAction;
import de.fearnixx.jeak.channelmgmt.overflow.state.OverflowState;
import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class OverflowStrategy<T extends OverflowState> {

    private static final Logger logger = LoggerFactory.getLogger(OverflowStrategy.class);

    private final IDataHolder channelProperties = new BasicDataHolder();
    private final Map<String, Integer> channelPermissions = new HashMap<>();
    private final List<String> enforcements = new LinkedList<>();
    private Integer minChildren;

    public void eval(IChannel parent, OverflowAction action) {
        T state = createState(parent);
        parent.getSubChannels()
                .stream()
                .filter(this::matches)
                .forEach(child -> countChildren(child, state));

        if (state.freeChildren().get() < minChildren) {
            Optional<IDataHolder> optChannel = createChannel(state);
            optChannel.ifPresent(channel -> action.create(channel, getEnforcements(), getPermissions()));

        } else {
            deleteExceeding(action, state);
        }
    }

    protected void countChildren(IChannel child, T state) {
        state.numberOfChildren().incrementAndGet();
        state.trackChannel(child);

        if (child.getClientCount() == 0) {
            state.freeChildren().incrementAndGet();
        } else {
            state.occupiedChildren().incrementAndGet();
        }
    }

    protected void deleteExceeding(OverflowAction action, T state) {
        final LinkedList<IChannel> trackedChildrenCopy = new LinkedList<>(state.getChannelsTracked());
        trackedChildrenCopy
                .stream()
                .filter(channel -> channel.getClientCount() == 0)
                .skip(minChildren)
                .map(IChannel::getID)
                .forEach(id -> {
                    state.freeChildren().decrementAndGet();
                    state.untrackChannel(id);
                    deleteChannel(id, action, state);
                });
    }

    protected void deleteChannel(Integer channelId, OverflowAction action, T state) {
        action.delete(channelId);
    }

    protected abstract boolean matches(IChannel child);

    protected abstract T createState(IChannel parent);

    protected abstract Optional<IDataHolder> createChannel(T state);

    protected IDataHolder getChannelProperties() {
        return channelProperties;
    }

    private Map<String, Integer> getPermissions() {
        return channelPermissions;
    }

    public boolean fromNode(IConfigNode node) {
        minChildren = node.getNode("min-children").optInteger(1);
        IConfigNode propNode = node.getNode("channelProperties");
        if (propNode.isMap()) {
             propNode.asValueMap(String.class).forEach(channelProperties::setProperty);
        }
        IConfigNode permNode = node.getNode("channelPermissions");
        if (permNode.isMap()) {
            permNode.asValueMap(Integer.class).forEach(channelPermissions::put);
        }

        node.getNode("enforcements")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(IConfigNode::isPrimitive)
                .map(IValueHolder::asString)
                .forEach(enforcements::add);

        return true;
    }

    public List<String> getEnforcements() {
        return Collections.unmodifiableList(enforcements);
    }
}
